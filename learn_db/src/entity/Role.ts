import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToMany, UpdateDateColumn } from "typeorm"
import { User } from "./User"

@Entity()
export class Role {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    updated: Date

    @ManyToMany(() => User, (user) => user.roles, { cascade: true })
    users: User[]
}
